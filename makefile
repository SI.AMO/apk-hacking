# APK Hacking!
# usage in README.text

# must be set to the correct values
apkname = "xxx.apk"
apk_package_name = "yyy.android"

# key configuration
keyAlias = "k_alias"
keyPass  = "7777777"
keyPath  = "release-key.keystor"

# to make new key
# What is your first and last name?
# What is the name of your organizational unit?
# What is the name of your organization?
# What is the name of your City or Locality?
# What is the name of your State or Province?
# What is the two-letter country code for this unit?
key_first_last_name = "AAA BBB"
key_org_unit = "unit0"
key_org_name = "org name .co"
key_city = "city"
key_state_name = "my province"
key_country_code = "42"

# adb path
# use /path/to/genymotion/tools/adb for genymotion
adbPath = "/usr/bin/adb"

src_dir_name = "SRC"
output_apk = "a.out.apk"

# newline character
NL = "\n"


main:
	rm -f $(output_apk) tmp.apk 2>/dev/null
	apktool b -f -d $(src_dir_name) -o tmp.apk
	echo $(keyPass) | jarsigner -verbose -sigalg \
		SHA1withRSA -digestalg SHA1 -keystore \
		$(keyPath) tmp.apk $(keyAlias)
	zipalign -v 4 tmp.apk $(output_apk)
	rm -f tmp.apk 2>/dev/null


d:
	rm -rf $(src_dir_name)
	apktool d $(apkname) -f -o $(src_dir_name)


mkkey:
	echo $(keyPass)$(NL)$(keyPass)$(NL)$(key_first_last_name)\
		$(NL)$(key_org_unit)$(NL)$(key_org_name)$(NL)$(key_city)\
		$(NL)$(key_state_name)$(NL)$(key_country_code)$(NL)'yes' |\
		keytool -genkey -v -keystore $(keyPath) \
			-alias $(keyAlias) -keyalg RSA  \
			-keysize 2048 -validity 10000


install:
	$(adbPath) uninstall $(apk_package_name)
	$(adbPath) install $(output_apk)
