===================
    apk hacking   
===================

this is a simple makefile, to hack apk files
decompile - make key - sign - zip align - build - install/test



DEPENDENCIES
------------

 - apktool
 - keytool
 - jarsigner
 - zipalign
 - adb (to install apk)



HOW TO USE
----------

 - edit makefile and set `apkname` and `apk_package_name`
   corresponding to your apk file.
   * to get `apk_package_name` you can first decompile
     your apk using `make d` and see `SRC/AndroidManifest.xml`

 - decompile apk using `make d`

 - make your changes

 - make a sign key if you don't have one, by `make mkkey`
   * to use an existing key, set your key path `keyPath` 
     in the makefile

 - build and sign apk using `make`

 - test new apk by `make install`
   * to test on genymotion, change `adbPath`
     in the makefile as explaned there
